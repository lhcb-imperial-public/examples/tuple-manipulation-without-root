# Tuple manipulation without ROOT

An example of working with tuples without needing ROOT. Read the file python/runme.py

## To run
* source setup.sh # (this will setup an environment with the uproot package installed)
* python python/runme.py

For full documentation see https://uproot.readthedocs.io/en/latest/basic.html
