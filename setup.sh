#This sets up a nice environment on the lxplus-like machines
if [ -f /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh ]; then
  source /cvmfs/sft.cern.ch/lcg/views/LCG_105/x86_64-el9-gcc13-opt/setup.sh
fi

if [ ! -d uprootenv ]; then
  echo "################################################################################"
  echo "#                                                                              #"
  echo "#                   Making a new virtual python environment                    #"
  echo "#                                                                              #"
  echo "################################################################################"

  python3 -m venv uprootenv

  . uprootenv/bin/activate

  echo "################################################################################"
  echo "#                                                                              #"
  echo "#                       Installing the required packages                       #"
  echo "#                                                                              #"
  echo "################################################################################"

  pip3 install uproot awkward



else
  . uprootenv/bin/activate
fi

