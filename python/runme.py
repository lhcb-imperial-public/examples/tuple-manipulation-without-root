import uproot


filename = 'data/example.root'


print('''Example of reading a file "{}"'''.format(filename))
with uproot.open(filename) as infile:

  print('''What objects are in the file?''')
  print(infile.keys())

  print('''We want to look at DataTuple''')
  data = infile['DataTuple']

  print('''We can look at the structure of this tuple''')
  data.show()
  
  print('''We can access branches as arrays''')
  B_mass = data.arrays('B_M')
  print(B_mass)

  print('''Or we can iterate through the tree''')
  event = 0
  for row in data.iterate(step_size=1):
    print(event, row)
    event += 1
    if event >= 10:
      break
      
  print('''For more on the library usage see https://uproot.readthedocs.io/en/latest/basic.html''')
